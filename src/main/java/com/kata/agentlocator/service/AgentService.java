package com.kata.agentlocator.service;

import com.kata.agentlocator.dao.AgentDao;
import com.kata.agentlocator.model.Agent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AgentService {

    @Autowired
    private AgentDao agentDao;

    public List<Agent> getAllAgents() {
        List<Agent> agents = this.agentDao.findAll();
        return agents;
    }

    public List<Agent> getAgentsByGender(String gender) {
        List<Agent> agents = this.agentDao.findAgentsByGender(gender);
        return agents;
    }

    public List<Agent> getAgentsByAgeLessThanEqual(int age) {
        List<Agent> agents = this.agentDao.findByAgeLessThanEqual(age);
        return agents;
    }

    public List<Agent> getAgentsWithNameContaining(String name) {
        List<Agent> agents = this.agentDao.findAgentsByNameContaining(name);
        return agents;
    }

    public Agent getAgentByName(String name) {
        Agent agents = this.agentDao.findAgentByName(name);
        return agents;
    }

}
