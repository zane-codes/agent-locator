package com.kata.agentlocator.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter @Setter @NoArgsConstructor
@Entity
public class Agent {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private long id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "LAT")
    private float latitude;
    @Column(name = "LONG")
    private float longitude;
    @Column(name = "AGE")
    private int age;
    @Column(name = "GENDER")
    private String gender;

    public Agent(String name, float latitude, float longitude, int age, String gender) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.age = age;
        this.gender = gender;
    }
}
