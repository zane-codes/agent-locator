package com.kata.agentlocator.control;

import com.kata.agentlocator.model.Agent;
import com.kata.agentlocator.service.AgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("api/agentlocator/v1")
public class AgentController {

    @Autowired
    AgentService agentService;

    @GetMapping("/allAgents")
    public List<Agent> getAllAgents() {
        return this.agentService.getAllAgents();
    }

    @GetMapping(path = "/allAgents", params="gender")
    public List<Agent> getAgentsByGender(@RequestParam("gender") String gender) {
        return this.agentService.getAgentsByGender(gender);
    }

    @GetMapping(path="/agent", params="name")
    public Agent getAgentByName(@RequestParam("name") String name) {
        return this.agentService.getAgentByName(name);
    }

    @GetMapping(path="/agent", params="nameContaining")
    public List<Agent> getAgentsWithNameContaining(@RequestParam("nameContaining") String name) {
        return this.agentService.getAgentsWithNameContaining(name);
    }

    @GetMapping(path="/allAgents", params = "maxAge")
    public List<Agent> getAgentsByAgeLessThanEqual(@RequestParam("maxAge") int age) {
        return this.agentService.getAgentsByAgeLessThanEqual(age);
    }


}
