package com.kata.agentlocator.dao;


import com.kata.agentlocator.model.Agent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface AgentDao extends JpaRepository<Agent, Long> {

    List<Agent> findAgentsByGender(String gender);
    List<Agent> findByAgeLessThanEqual(int age);
    List<Agent> findAgentsByNameContaining(String name);
    Agent findAgentByName(String name);


}
