import axios from 'axios'

export const mapRenderingService = axios.create({
    baseURL: 'http://localhost:8080/api/agentlocator/v1/'
})
