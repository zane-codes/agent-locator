import React, {Component} from 'react'
import {Map, Marker, TileLayer, Tooltip} from "react-leaflet";

import {mapRenderingService} from './MapRenderingService'

import Banner from '../../components/Banner/Banner'
import GenderFilter from '../../components/GenderFilterCard/GenderFilterCard'
import AgentCard from '../../components/AgentCard/AgentCard'
import SearchCard from '../../components/SearchCard/SearchCard'
import CoordinateCard from '../../components/CoordinateCard/CoordinateCard'

import './MapRendering.css'


export default class MapRendering extends Component {

    componentDidMount(): void {
        mapRenderingService.get('allAgents')
            .then(response => {
                this.setState({agentLocations: response.data})
                console.log(response)
            })
    }

    state = {
        lat: 39.98777,
        long: -83.03247,
        zoom: 4,

        agentLocations: [],
        showAllAgents: false,
        genderFilter: {
            showMaleAgents: false,
            showFemaleAgents: false,
        },
        selectedAgent: null,
        searchedAgent: false, // this was null

        searchedAgentsDynamic: false,
        allAgentsMatchingDynamicSearch: []
    }

    handlePositionChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
        event.preventDefault()
    }

    handleCoordinateChange = (event) => {
        event.preventDefault()
    }

    handleShowAllAgents = () => {
        this.setState({
            ...this.state,
            showAllAgents: true,
            genderFilter: {
                showMaleAgents: false,
                showFemaleAgents: false,
            },
        })
    }

    handleShowMaleAgents = () => {
        this.setState({
            ...this.state,
            showAllAgents: false,
            genderFilter: {
                showMaleAgents: true,
                showFemaleAgents: false,
            }
        })
    }

    handleShowFemaleAgents = () => {
        this.setState({
            ...this.state,
            showAllAgents: false,
            genderFilter: {
                showMaleAgents: false,
                showFemaleAgents: true,
            }
        })
    }

    handleResetGenderFilter = () => {
        this.setState({
            ...this.state,
            showAllAgents: false,
            genderFilter: {
                showMaleAgents: false,
                showFemaleAgents: false,
            },
            selectedAgent: null
        })
    }

    handleSelectedAgent = (agent) => {
        this.setState({selectedAgent: agent})
    }

    handleAgentSearch = (e) => {
        document.getElementById('dynamic').value=''
        mapRenderingService.get('agent?name=' + e.target.value)
            .then((response) => {
                this.setState({
                    ...this.state,
                    showAllAgents: false,
                    genderFilter: {
                        showMaleAgents: false,
                        showFemaleAgents: false,
                    },
                    selectedAgent: false,
                    searchedAgent: response.data,

                    searchedAgentsDynamic: false,
                    allAgentsMatchingDynamicSearch: []
                })
            })
    }

    handleDynamicAgentSearch = (e) => {
        document.getElementById('single').value=''
        mapRenderingService.get('agent?nameContaining=' + e.target.value)
            .then((response) => {
                this.setState({
                    ...this.state,
                    showAllAgents: false,
                    genderFilter: {
                        showMaleAgents: false,
                        showFemaleAgents: false,
                    },
                    selectedAgent: false,
                    searchedAgent: null,

                    searchedAgentsDynamic: true,
                    allAgentsMatchingDynamicSearch: response.data

                })
            })
    }

    render() {


        const agentLocations = this.state.agentLocations.map((agent) => {
            return (
                <div key={agent.id}>
                    <Marker position={[agent.latitude, agent.longitude]}
                            onClick={() => this.handleSelectedAgent(agent)}>
                        <Tooltip>
                            <h5>Agent Info</h5>
                            <p>Name: {agent.name}</p>
                            <p>Lat & Long: {agent.latitude}, {agent.longitude}</p>
                        </Tooltip>
                    </Marker>
                </div>
            )
        })

        const agentLocation = () => {
            if (this.state.searchedAgent) {
                return (
                    <div key={this.state.searchedAgent.id}>
                        <Marker
                            position={[this.state.searchedAgent.latitude, this.state.searchedAgent.longitude]}
                            onClick={() => this.handleSelectedAgent(this.state.searchedAgent)}>
                            <Tooltip>
                                <h5>Agent Info</h5>
                                <p>Name: {this.state.searchedAgent.name}</p>
                                <p>Lat &
                                    Long: {this.state.searchedAgent.latitude}, {this.state.searchedAgent.longitude}</p>
                            </Tooltip>
                        </Marker>
                    </div>
                )
            }
        }

        const dynamicAgentsLocation = this.state.allAgentsMatchingDynamicSearch.map((agent) => {
                return (
                    <div key={agent.id}>
                        <Marker position={[agent.latitude, agent.longitude]}
                                onClick={() => this.handleSelectedAgent(agent)}>
                            <Tooltip>
                                <h5>Agent Info</h5>
                                <p>Name: {agent.name}</p>
                                <p>Lat & Long: {agent.latitude}, {agent.longitude}</p>
                            </Tooltip>
                        </Marker>
                    </div>
                )
            })


        const maleAgentLocations = this.state.agentLocations.filter((agent) => {
            return (agent.gender === 'Male')
        }).map((agent) => {
            return (
                <div key={agent.id}>
                    <Marker position={[agent.latitude, agent.longitude]}
                            onClick={() => this.handleSelectedAgent(agent)}>
                        <Tooltip>
                            <h5>Agent Info</h5>
                            <p>Name: {agent.name}</p>
                            <p>Lat & Long: {agent.latitude}, {agent.longitude}</p>
                        </Tooltip>
                    </Marker>
                </div>
            )
        })


        const femaleAgentLocations = this.state.agentLocations.filter((agent) => {
            return (agent.gender === 'Female')
        }).map((agent) => {
            return (
                <div key={agent.id}>
                    <Marker position={[agent.latitude, agent.longitude]}
                            onClick={() => this.handleSelectedAgent(agent)}>
                        <Tooltip>
                            <h5>Agent Info</h5>
                            <p>Name: {agent.name}</p>
                            <p>Lat & Long: {agent.latitude}, {agent.longitude}</p>
                        </Tooltip>
                    </Marker>
                </div>
            )
        })

        return (

            <div>
                <Banner/>
                <section className="CardContainer">
                    <GenderFilter onShowAllAgents={this.handleShowAllAgents}
                                  onShowAllFemaleAgents={this.handleShowFemaleAgents}
                                  onShowAllMaleAgents={this.handleShowMaleAgents}
                                  onResetGenderFilter={this.handleResetGenderFilter}
                                  filterApplied={!this.state.genderFilter.showFemaleAgents &&
                                  !this.state.genderFilter.showMaleAgents &&
                                  !this.state.showAllAgents
                                  }
                    />
                    <SearchCard
                        onSearchAgent={this.handleAgentSearch}
                        onDynamicSearch={this.handleDynamicAgentSearch}
                    />
                    <CoordinateCard
                        onCooridnateChange={this.handleCoordinateChange}
                        onLatitudeChange={this.handlePositionChange}
                        onLongitudeChange={this.handlePositionChange}
                    />
                    {(this.state.selectedAgent) ?

                        <AgentCard
                            agentName={this.state.selectedAgent !== null ? this.state.selectedAgent.name : null}
                            agentGender={this.state.selectedAgent !== null ? this.state.selectedAgent.gender : null}
                            agentAge={this.state.selectedAgent !== null ? this.state.selectedAgent.age : null}/>
                        : null
                    }
                </section>


                <div>
                    <Map center={[this.state.lat, this.state.long]} zoom={this.state.zoom}>
                        <TileLayer
                            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        />
                        {this.state.showAllAgents ? agentLocations : null}
                        {this.state.genderFilter.showMaleAgents ? maleAgentLocations : null}
                        {this.state.genderFilter.showFemaleAgents ? femaleAgentLocations : null}
                        {this.state.searchedAgent ? agentLocation() : null}
                        {this.state.searchedAgentsDynamic ? dynamicAgentsLocation : null}
                    </Map>

                </div>
            </div>
        )
    }
}


