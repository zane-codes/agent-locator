import React from 'react'
import './SearchCard.css'

const searchCard = (props) => {

    return (

        <article className="SearchCard">
            <h3>Agent Search</h3>
            <table>
                <tbody>
                <tr>
                    <td>Single Agent Search:</td>
                    <td>
                        <input id='single' type="text" onChange={props.onSearchAgent}/>
                    </td>
                </tr>
                <tr>
                    <td>Dynamic Agent Search:</td>
                    <td>
                        <input id='dynamic' type="text" onChange={props.onDynamicSearch}/>
                    </td>
                </tr>
                </tbody>
            </table>
        </article>
    )
}

export default searchCard