import React from 'react'
import './AgentCard.css'

const agentCard = (props) => {

    return (
        <article className="AgentCard">
            <h3>Agent Information</h3>
            <p>Name: {props.agentName}</p>
            <p>Gender: {props.agentGender}</p>
            <p>Age: {props.agentAge}</p>
        </article>
    )

}

export default agentCard