import React from 'react'
import './GenderFilterCard.css'


const genderFilterCard = (props) => {

    return (

        <article className="GenderCard">
            <h3>Gender Filter</h3>
            <label>
                <input name="genderFilter" type="radio" onClick={props.onShowAllAgents}/> Show All Agents
            </label>
            <label>
                <input name="genderFilter" type="radio" onClick={props.onShowAllMaleAgents}/> Show Male
                Agents
            </label>
            <label>
                <input name="genderFilter" type="radio" onClick={props.onShowAllFemaleAgents}/> Show Female Agents
            </label>

            {/* you can use a hidden radio and a separate button to set the checked field when another status is true*/}
            {/* left the onchange function to get rid of a warning */}
            <input name="genderFilter" type="radio" hidden
                   checked={props.filterApplied} onChange={() => {}}
            />
            <button
                onClick={props.onResetGenderFilter}>Reset Filters</button>
        </article>
    )
}

export default genderFilterCard