import React from 'react'
import './CoordinateCard.css'

const coordinateCard = (props) => {

    return (
        <article className="CoordinateCard" onSubmit={props.onSubmitCoordianteChange}>
            <h3>Coordinate Locator</h3>
            <table>
                <tbody>
                <tr>
                    <td>Latitude:</td>
                    <td><input type="text" name="lat" onChange={props.onLatitudeChange}/></td>
                </tr>
                <tr>
                    <td>Longitude:</td>
                    <td><input type="text" name="long" onChange={props.onLongitudeChange}/></td>
                </tr>
                </tbody>
            </table>
        </article>
    )
}

export default coordinateCard