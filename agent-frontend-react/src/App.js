import React, {Component} from 'react';
import MapRendering from './containers/MapRendering/MapRendering'
import './App.css'

export default class App extends Component {
    render() {
        return (
            <div>
                <MapRendering />
            </div>
        )
    }
}
